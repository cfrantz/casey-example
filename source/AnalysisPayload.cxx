// stdlib functionality
#include <iostream>
// ROOT functionality
#include <TFile.h>
#include <TH1.h>
#include <TCanvas.h>
#include <TString.h>
// ATLAS EDM functionality
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"

int main() {

  // initialize the xAOD EDM
  xAOD::Init();

  // open the input file
  TString inputFilePath = "/home/atlas/Bootcamp/Data/DAOD_EXOT27.17882744._000026.pool.root.1";
  xAOD::TEvent event;
  std::unique_ptr<TFile> iFile (TFile::Open(inputFilePath, "READ"));
  event.readFrom(iFile.get());

  // for counting events
  unsigned count = 0;

  // get the number of events in the file to loop over
  const Long64_t numEntries = event.getEntries();

  /************************************************************/
  /***********        Create the histograms       ************/
  Double_t xlow_1     = 0;
  Double_t xhigh_1    = 20;
  Int_t    numbinsx_1 = Int_t(xhigh_1-xlow_1);
  
  TH1I *h_njets = new TH1I("h1", "h1 title", numbinsx_1, xlow_1, xhigh_1);
  h_njets->GetXaxis()->SetTitle("Number of Jets Per Event");
  h_njets->GetYaxis()->SetTitle("Number of Events Per Bin");
  
  Double_t xlow_2     = 0;
  Double_t xhigh_2    = 500;
  Int_t numbinsx_2    = 100;

  TH1D *h_dijet_inv_mass = new TH1D("h2", "h2 title", numbinsx_2, xlow_2, xhigh_2);

  h_dijet_inv_mass->GetXaxis()->SetTitle("Dijet Invariant Mass [GeV]");
  h_dijet_inv_mass->GetYaxis()->SetTitle("Number of Events Per Bin");

  Int_t count_jetloop = 0;

  /************************************************************/
  /************************************************************/

    
  // primary event loop
  for ( Long64_t i=0; i<numEntries; ++i ) {

    // Load the event
    event.getEntry( i );

    // Load xAOD::EventInfo and print the event info
    const xAOD::EventInfo * ei = nullptr;
    event.retrieve( ei, "EventInfo" );
    std::cout << "Processing run # " << ei->runNumber() << ", event # " << ei->eventNumber() << std::endl;

    // retrieve the jet container from the event store
    const xAOD::JetContainer* jets = nullptr;
    event.retrieve(jets, "AntiKt4EMTopoJets");

    count_jetloop = 0;
    std::vector<xAOD::Jet> jets_vec;
    // BEGIN JET LOOP
    for(const xAOD::Jet* jet : *jets) { 
    // loop through all of the jets and make selections with the helper
      // print the kinematics of each jet in the event
      //std::cout << "Jet : pt=" << jet->pt() << "  eta=" << jet->eta() << "  phi=" << jet->phi() << "  m=" << jet->m() << std::endl;

      jets_vec.push_back(*jet);

      count_jetloop++;
      
    }
    // END JET LOOP

    if(jets_vec.size()>1){
      Double_t dijet_mass_in_GeV = ( jets_vec.at(0).p4().M() + jets_vec.at(1).p4().M() )/1000.0 ;
      h_dijet_inv_mass->Fill(dijet_mass_in_GeV);
    }
    h_njets->Fill(count_jetloop);
    

    // counter for the number of events analyzed thus far
    count += 1;
  }

  /************************************************************/
  /****************    Draw and write histos    ***************/ 
  
  TString outputfile1 = "output_of_analysisPayload.pdf(";
  TString outputfile2 = "output_of_analysisPayload.pdf)";
  TCanvas c1 = TCanvas("canv1","canv1",0,0,800,600);
  c1.cd();
  h_njets->Draw();
  c1.Print(outputfile1);

  TCanvas c2 = TCanvas("canv2","canv2",0,0,800,600);
  c2.cd();	   
  h_dijet_inv_mass->Draw();
  c2.Print(outputfile2);

  c1.Close();
  c2.Close();
  
  /************************************************************/
  /************************************************************/
  
  return 0;
}
